#include "OAsmRow.h"

OAsmRow::OAsmRow() {
	row = new CStringList();
};

OAsmRow::OAsmRow(CString word) {
	OAsmRow::OAsmRow();
	row->AddHead(word);
};

OAsmRow::~OAsmRow() {
	if (!row->IsEmpty())
		row->RemoveAll();
	delete row;
};

int OAsmRow::lenght() {
	return row->GetCount();
};

void OAsmRow::addWord(CString word) {
	row->AddTail(word);

	// TODO: Implement Assistent inteligency
};

CString OAsmRow::getWord(POSITION index) {
	return row->GetAt(index);
};

CString OAsmRow::getFirst() {
	return row->GetHead();
};

CString OAsmRow::getLast() {
	return row->GetTail();
};