#include <afxcoll.h>

#ifndef OASMROW_H
#define OASMROW_H

class OAsmRow {

public:
	OAsmRow();
	OAsmRow(CString);
	~OAsmRow();

	void addWord(CString);
	CString getWord(POSITION);
	CString getFirst();
	CString getLast();
	int lenght();

private:
	CStringList *row;
};

#endif