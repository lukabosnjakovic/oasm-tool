#include "OAsmModule.h"

OAsmModule::OAsmModule() {
	rows = new CList<OAsmRow, OAsmRow>();
	modules = new CList<OAsmModule, OAsmModule>();
};

OAsmModule::OAsmModule(OAsmRow row) {
	rows = new CList<OAsmRow, OAsmRow>();
	rows->AddHead(row);
	modules = new CList<OAsmModule, OAsmModule>();
};

OAsmModule::~OAsmModule() {
	if (!rows->IsEmpty())
		rows->RemoveAll();
	delete rows;
	if (!modules->IsEmpty())
		modules->RemoveAll();
	delete modules;
};