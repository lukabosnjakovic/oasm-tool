#include <afxtempl.h>
#include "OAsmRow.h"

#ifndef OASMMODULE_H
#define OASMMODULE_H

class OAsmModule {

public:
	OAsmModule();
	OAsmModule(OAsmRow);
	~OAsmModule();

private:
	CList<OAsmRow, OAsmRow> *rows;
	CList<OAsmModule, OAsmModule> *modules;
};

#endif
